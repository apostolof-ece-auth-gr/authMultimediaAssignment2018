function AACSeq1 = AACoder1(fNameIn)
%Implementation of AAC encoder
%   Usage AACSeq1 = AACoder1(fNameIn), where:
%       Inputs
%       - fNameIn is the filename and path of the file to encode
%
%       Output
%        - AACSeq1 is an array of structs containing K structs, where K is
%        the number of computed frames. Every struct of the array consists
%        of a frameType, a winType, chl.frameF which are the MDCT
%        coefficients of this frame's left channel, chr.frameF which are
%        the MDCT coefficients of this frame's right channel

    % Declares constant window type
    WINDOW_TYPE = 'KBD';

    % Reads the audio file
    [originalAudioData, ~] = audioread(fNameIn);

    % Splits the audio in frames and determines the type of each frame
    frameTypes{ceil(length(originalAudioData) / 1024), 1} = 'OLS';
    frameTypes{1} = 'OLS';
    for i = 1:length(frameTypes) - 2
        nextFrameStart = (i + 1) * 1024 + 1;
        nextFrameStop = nextFrameStart + 2047;
        if nextFrameStop > length(originalAudioData)
            tmpZeroPadded = originalAudioData(nextFrameStart:length(originalAudioData), :);
            tmpZeroPadded = padarray(tmpZeroPadded, [(2048 - length(tmpZeroPadded)) 0], 'post');
            frameTypes{i+1} = SSC(1, tmpZeroPadded, frameTypes{i});
            
            clearvars tmpZeroPadded
        else
            frameTypes{i+1} = SSC(1, originalAudioData(nextFrameStart:nextFrameStop, :), frameTypes{i});
        end
    end

    % Assigns a type to the last frame
    if strcmp(frameTypes{length(frameTypes) - 1}, 'LSS')
        frameTypes{length(frameTypes)} = 'ESH';
    elseif strcmp(frameTypes{length(frameTypes) - 1}, 'ESH')
        frameTypes{length(frameTypes)} = 'ESH';
    else
        frameTypes{length(frameTypes)} = 'OLS';
    end

    % Encodes audio file
    AACSeq1(length(frameTypes), 1) = struct;
    for i = 0:length(frameTypes) - 1
        currFrameStart = i * 1024 + 1;
        currFrameStop = currFrameStart + 2047;

        if currFrameStop > length(originalAudioData)
            tmpZeroPadded = originalAudioData(nextFrameStart:length(originalAudioData), :);
            tmpZeroPadded = padarray(tmpZeroPadded, [(2048 - length(tmpZeroPadded)) 0], 'post');
            frameF = filterbank(tmpZeroPadded, frameTypes{i+1}, WINDOW_TYPE);
            
            clearvars tmpZeroPadded
        else
            frameF = filterbank(originalAudioData(currFrameStart:currFrameStop, :), frameTypes{i+1}, WINDOW_TYPE);
        end

        AACSeq1(i + 1).frameType = frameTypes(i + 1);
        AACSeq1(i + 1).winType = WINDOW_TYPE;
        AACSeq1(i + 1).chl.frameF = frameF(:, 1);
        AACSeq1(i + 1).chr.frameF = frameF(:, 2);
    end
end
