function x = iAACoder1(AACSeq1, fNameOut)
%Implementation of AAC decoder
%   Usage x = iAACoder1(AACSeq1, fNameOut), where:
%       Inputs
%       - fNameOut is the filename and path of the file that will be
%       written after decoding
%        - AACSeq1 is an array of structs containing K structs, where K is
%        the number of computed frames. Every struct of the array consists
%        of a frameType, a winType, chl.frameF which are the MDCT
%        coefficients of this frame's left channel, chr.frameF which are
%        the MDCT coefficients of this frame's right channel
%
%       Output
%       - x is an array containing the decoded audio samples

    % Initializes an array to hold the decoded samples
    decodedAudio(1024 * (length(AACSeq1) + 1), 2) = 0;
    % Initializes an array to hold both audio channels
    frameF(1024, 2) = 0;
    
    % Decodes audio file
    for i = 0:length(AACSeq1) - 1
        currFrameStart = i * 1024 + 1;
        currFrameStop = currFrameStart + 2047;
        frameF(:, 1) = AACSeq1(i+1).chl.frameF;
        frameF(:, 2) = AACSeq1(i+1).chr.frameF;
        frameT = iFilterbank(frameF, AACSeq1(i+1).frameType, AACSeq1(i+1).winType);

        decodedAudio(currFrameStart:currFrameStop, :) = decodedAudio(currFrameStart:currFrameStop, :) + frameT;
    end

    audiowrite(fNameOut, decodedAudio, 48000);
    x = decodedAudio;
end
