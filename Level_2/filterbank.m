function frameF = filterbank(frameT, frameType, winType)
%Implementation of the Filter Bank step
%   Usage frameF = filterbank(frameT, frameType, winType), where:
%       Inputs
%       - frameT is a frame in the time domain, containing both channels of
%           the audio stored in an array of dimensions 2048X2
%       - frameType is the type of the current frame in string
%       representation, can be one of "OLS" (ONLY_LONG_SEQUENCE), "LSS"
%       (LONG_START_SEQUENCE), "ESH" (EIGHT_SHORT_SEQUENCE), "LPS"
%       (LONG_STOP_SEQUENCE)
%       - winType is the type of the window selected, can be one of "KBD",
%       "SIN"
%
%       Output
%       - frameF is the frame in the frequency domain, in MDCT coefficients
%       representation containing both channels of the audio stored in an
%       array of dimensions 1024X2

    % Declares persistent windows variables and initializes if empty
    persistent kaiserWindowLong kaiserWindowShort sinWindowLong sinWindowShort;
    if isempty(kaiserWindowLong) || isempty(kaiserWindowShort) || ...
            isempty(sinWindowLong) || isempty(sinWindowShort)
        kaiserLong = kaiser(1025, 6*pi);
        kaiserSumLong = sum(kaiserLong);
        kaiserShort = kaiser(129, 4*pi);
        kaiserSumShort = sum(kaiserShort);

        kaiserWindowLong(1:1024, 1) = movsum(kaiserLong(1:1024), [1024 0]);
        kaiserWindowLong(1025:2048, 1) = movsum(flipud(kaiserLong(1:1024)), [0 1024]);
        kaiserWindowLong = sqrt(kaiserWindowLong ./ kaiserSumLong);

        sinWindowLong = sin(pi * ((0:2047) + 0.5) / 2048)';

        kaiserWindowShort(1:128, 1) = movsum(kaiserShort(1:128), [128 0]);
        kaiserWindowShort(129:256, 1) = movsum(flipud(kaiserShort(1:128)), [0 128]);
        kaiserWindowShort = sqrt(kaiserWindowShort ./ kaiserSumShort);

        sinWindowShort = sin(pi * ((0:255) + 0.5) / 256)';
    end

    % Initializes output array
    frameF(1024, 2) = 0;

    % Applies appropriate window to the frame
    if strcmp(frameType, 'OLS')
        if strcmp(winType, 'KBD')
            frameT = bsxfun(@times, frameT, kaiserWindowLong);
        elseif strcmp(winType, 'SIN')
            frameT = bsxfun(@times, frameT, sinWindowLong);
        else
            error('filterbank, l[51]: Unsupported window type input!')
        end

        frameF = mdct4(frameT);
    elseif strcmp(frameType, 'LSS')
        if strcmp(winType, 'KBD')
            frameT(1:1024, :) = bsxfun(@times, frameT(1:1024, :), kaiserWindowLong(1:1024));
            frameT(1473:1600, :) = bsxfun(@times, frameT(1473:1600, :), kaiserWindowShort(129:end));
            frameT(1601:end, :) = 0;
        elseif strcmp(winType, 'SIN')
            frameT(1:1024, :) = bsxfun(@times, frameT(1:1024, :), sinWindowLong(1:1024));
            frameT(1473:1600, :) = bsxfun(@times, frameT(1473:1600, :), sinWindowShort(129:end));
            frameT(1601:end, :) = 0;
        else
            error('filterbank, l[65]: Unsupported window type input!')
        end

        frameF = mdct4(frameT);
    elseif strcmp(frameType, 'LPS')
        if strcmp(winType, 'KBD')
            frameT(1:448, :) = 0;
            frameT(449:576, :) = bsxfun(@times, frameT(449:576, :), kaiserWindowShort(1:128));
            frameT(1025:end, :) = bsxfun(@times, frameT(1025:end, :), kaiserWindowLong(1025:end));
        elseif strcmp(winType, 'SIN')
            frameT(1:448, :) = 0;
            frameT(449:576, :) = bsxfun(@times, frameT(449:576, :), sinWindowShort(1:128));
            frameT(1025:end, :) = bsxfun(@times, frameT(1025:end, :), sinWindowLong(1025:end));
        else
            error('filterbank, l[79]: Unsupported window type input!')
        end

        frameF = mdct4(frameT);
    elseif strcmp(frameType, 'ESH')
        for channel = 1:2
            % Splits the frame into sub-frames
            [subFrames, ~] = buffer(frameT(449:end-448, channel), 256, 128, 'nodelay');

            if strcmp(winType, 'KBD')
                subFrames = subFrames .* repmat(kaiserWindowShort, [1 8]);
            elseif strcmp(winType, 'SIN')
                subFrames = subFrames .* repmat(sinWindowShort, [1 8]);
            end

            for subFrameIndex = 1:8
                frameF((subFrameIndex - 1) * 128 + 1:subFrameIndex * 128, channel) = ...
                    mdct4(subFrames(:, subFrameIndex));
            end
        end
    end
end
