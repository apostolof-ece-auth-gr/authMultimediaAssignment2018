function SNR = demoAAC2(fNameIn, fNameOut)
%Function that demonstrates usage of the level 2 code
%   Usage SNR = demoAAC2(fNameIn, fNameOut), where:
%       Inputs
%       - fNameIn is the filename and path of the file to encode
%       - fNameOut is the filename and path of the file that will be
%       written after decoding
%
%       Output
%       - SNR is the signal to noise ration computed after successively
%       encoding and decoding the audio signal

    AACSeq2 = AACoder2(fNameIn);
    decodedAudio = iAACoder2(AACSeq2, fNameOut);

    [audioData, ~] = audioread(fNameIn);

    SNR = snr(audioData(1025:end-1378, :), audioData(1025:end-1378, :) - decodedAudio(1025:length(audioData)-1378, :));
end
