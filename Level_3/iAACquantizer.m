function frameF = iAACquantizer(S, sfc, G, frameType)
%Implementation of the inverse Quantizer
%   Usage frameF = iAACquantizer(S, sfc, G, frameType), where:
%       Inputs
%        - S are the MDCT quantization symbols of one audio channel stored
%        in a vector of length 1024
%        - sfc are the scalefactors per band stored in an array of
%        dimensions NBX8 for EIGHT_SHORT_SEQUENCE frames and NBX1
%        otherwise, where NB is the number of bands
%        - G is the global gain stored in an array of dimensions 1X8 for
%        EIGHT_SHORT_SEQUENCE frames and a single value otherwise
%       - frameType is the type of the current frame in string
%       representation, can be one of "OLS" (ONLY_LONG_SEQUENCE), "LSS"
%       (LONG_START_SEQUENCE), "ESH" (EIGHT_SHORT_SEQUENCE), "LPS"
%       (LONG_STOP_SEQUENCE)
%
%       Output
%       - frameF is the frame in the frequency domain, in MDCT coefficients
%       representation containing only one of the audio channels stored in
%       a vector of length 1024

    % Declares constant numbers of bands for long and short windows
    LONG_WINDOW_NUMBER_OF_BANDS = 69;
    SHORT_WINDOW_NUMBER_OF_BANDS = 42;

    % Declares persistent variable holding the TNS tables and initializes if empty
    persistent TNSTables;
    if isempty(TNSTables)
        TNSTables = load('TableB219.mat');
    end

    if ~strcmp(frameType, 'ESH')
        for band = 1:LONG_WINDOW_NUMBER_OF_BANDS
            frameWlow = TNSTables.B219a(band, 2) + 1;
            frameWhigh = TNSTables.B219a(band, 3) + 1;
            subFrameS = S(frameWlow:frameWhigh);

            if band > 1
                sfc(band) = sfc(band) + sfc(band - 1);
            end
            frameF(frameWlow:frameWhigh) = sign(subFrameS) .* ...
                abs(subFrameS) .^ (4 / 3) .* 2 ^ (sfc(band) / 4);
        end
    else
        for subFrameIndex = 1:8
            currFrameStart = (subFrameIndex - 1) * 128 + 1;
            currFrameStop = currFrameStart + 127;
            subFrame = S(currFrameStart:currFrameStop);

            for band = 1:SHORT_WINDOW_NUMBER_OF_BANDS
                frameWlow = TNSTables.B219b(band, 2);
                frameWhigh = TNSTables.B219b(band, 3);
                subFrameS = subFrame(frameWlow + 1:frameWhigh + 1);

                if band > 1
                    sfc(band, subFrameIndex) = sfc(band, subFrameIndex) + ...
                        sfc(band - 1, subFrameIndex);
                end

                frameF(currFrameStart + frameWlow:currFrameStart + frameWhigh) = sign(subFrameS) .* ...
                    abs(subFrameS) .^ (4 / 3) .* 2 ^ (sfc(band) / 4);
            end
        end
    end
end
