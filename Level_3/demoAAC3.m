function [SNR, bitrate, compression] = demoAAC3(fNameIn, fNameOut, frameAACoded)
%Function that demonstrates usage of the level 3 code
%   Usage [SNR, bitrate, compression] = demoAAC3(fNameIn, fNameOut, frameAACoded), where:
%       Inputs
%       - fNameIn is the filename and path of the file to encode
%       - fNameOut is the filename and path of the wav file that will be
%       written after decoding
%       - frameAACoded is the filename and path of the mat file that will
%       be written after encoding
%
%       Output
%       - SNR is the signal to noise ration computed after successively
%       encoding and decoding the audio signal
%       - bitrate is the bits per second
%       - compression is the ratio of the bitrate before the encoding to
%       the bitrate after it

    AACSeq3 = AACoder3(fNameIn, frameAACoded);
    
    totalSize = 0; % In bits
    for frameIndex = 1:length(AACSeq3)
        totalSize = totalSize + 2; % frameType
        totalSize = totalSize + 2 * 4 * 4; % TNS coefficients
        totalSize = totalSize + length(AACSeq3(frameIndex).chl.G) * 16;
        totalSize = totalSize + length(AACSeq3(frameIndex).chr.G) * 16;
        totalSize = totalSize + length(AACSeq3(frameIndex).chl.sfc);
        totalSize = totalSize + length(AACSeq3(frameIndex).chr.sfc);
        totalSize = totalSize + length(AACSeq3(frameIndex).chl.stream);
        totalSize = totalSize + length(AACSeq3(frameIndex).chr.stream);
        totalSize = totalSize + 4; % Codebook of left channel
        totalSize = totalSize + 4; % Codebook of right channel
    end
    
    decodedAudio = iAACoder3(AACSeq3, fNameOut);
    bitrate = totalSize / (length(decodedAudio) / 48000);

    [audioData, ~] = audioread(fNameIn);
    compression = (2 * length(audioData) * 16) / (length(audioData) / 48000) / bitrate;
    
    SNR = snr(audioData(1025:end-1378, :), audioData(1025:end-1378, :) - decodedAudio(1025:length(audioData)-1378, :));
end
